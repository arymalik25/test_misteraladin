# test_misteraladin

test interview mister aladin

PROGRAMMING LANGUAGE
GO LANGUAGE 

jwt token
go get "github.com/dgrijalva/jwt-go" 
go get "github.com/go-sql-driver/mysql"

DATABASE : MARIADB
1. create databases misteraladin;
2. create table mst_shorturl( link_address varchar(255), link_short varchar(255), status int(1));

FILE

{GOPATH}/src/misteraladin/

-server
	-database.go
	-service.go
	-shorturl.go
-util
	-config.go
	-hash.go
	-generate.go
	-model.go
main.go
service.conf

NOTE: you can change the config from service.conf

CONSTANTA :
1. util/model
	- active = 1
	- delete = -1
	- inactive = 0
2. server/service
	- lengthurl = 5 //for random url

3. endpoint
	- /submiturl
	- /deleteurl
	- /geturl
4. username and password
	i put it in server/service.go at map.
	- user1, password1
	- user2, password2

RUNING
1. go run main.go
2. open postman or your browser
3. 127.0.0.1:8097/endpoint  // you can change port from the service.conf
 

Flow.
1. Signin for get token
	127.0.0.1:8097/signin
	
	request: 
	{
		"username" : "user1",	
		"password" : "password1"
	}

	response : token, expired time
	
2. Submit Url
	127.0.0.1:8097/submiturl

	request:
	{
		"linkaddress" : "www.misteraladin.com"	
	}
	
	response : code for shorten

3. Get Url
	127.0.0.1:8097/{your shorten code from submit}

	response : direct for the url


4. Delete Url
	127.0.0.1:8097

	request:
	{
		"linkshort" : {your link short}	
	}

	response: nil