package config

import (
	"encoding/json"
	"log"
	"os"
	"strconv"
	"strings"
)

type Configuration struct {
	ServiceName string
	ConfigData  map[string]string
}

//constanta for config package
const (
//configFile = "service.conf"
)

var configFile = "service.conf"
var OpenConfig Configuration
var localConfig = false

//Open String data
func Get(key string, defval string) string {
	//find the key in the current defval map
	//
	v, ok := OpenConfig.ConfigData[key]
	if !ok {
		if defval != "" {
			OpenConfig.ConfigData[key] = defval
			v = defval
		}
	}

	return v
}

//open array string
func GetA(key string, defval string) []string {

	v := Get(key, defval)

	if len(v) == 0 {
		return nil
	}

	sep := "," //separator
	if len(defval) == 1 {
		sep = defval
	}

	return strings.Split(v, sep)
}

//open integer
func GetI(key string, defval int) int {

	v := Get(key, strconv.Itoa(defval))

	if res, err := strconv.Atoi(v); err == nil {
		return res
	} else {
		return -1
	}
}

//Loads a standart config file.
//location in the same path as the app
//containing only servicename and confighost array
//
func (cfg *Configuration) open() error {
	cfgFile, err := os.Open(configFile)
	if err != nil {
		log.Println("[ERROR] cannot open configuration file")
		return err
	}
	defer cfgFile.Close()

	jsonParse := json.NewDecoder(cfgFile)
	err = jsonParse.Decode(&cfg)
	//log.Println("[DATA] configuration = ", cfg)
	return nil
}

func (cfg *Configuration) LoadConfig() bool {
	err := cfg.open()
	if err != nil {
		log.Println("[ERROR] cannot read service.conf file")
		return false
	}

	//if still not open
	if cfg.ServiceName == "" {
		return false
	}

	return true
}

//open other config file.
//
func (cfg *Configuration) LoadConfigFile(file string) bool {
	if file != "" {
		configFile = file
	}
	return cfg.LoadConfig()
}

func (cfg *Configuration) LoadConfigLocal() bool {
	localConfig = true
	return cfg.LoadConfig()
}
