package main

import (
	"fmt"
	"log"

	cfg "gitlab.com/arymalik/util/config"
)

func main() {
	ok := cfg.OpenConfig.LoadConfig()
	if !ok {
		log.Println(ok)
		return
	}

	svcport := cfg.Get("serviceport", "")
	port := cfg.GetA("dbhost", "")
	pwd := cfg.GetI("dbpwd", 0)
	// discHost := cfg.Get("servicename", "")
	fmt.Println(ok, svcport, port, pwd)
}
