package model

//status for record at database
type Status int32

const (
	Active   Status = 1
	Delete   Status = -1
	Inactive Status = 0
)
