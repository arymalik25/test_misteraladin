package hash

import (
	"log"

	"golang.org/x/crypto/bcrypt"
)

// //bcrypthash hashes password using bcrypt
// type bcryptHash struct {
// 	cost int
// }

// newbcrypthash return new bcrypt
// func NewBcryptHash(cost int) Hasher {
// return &bcryptHash{cost: cost}
// }

//hash password
func Hash(password []byte, cost int) []byte {
	hash, err := bcrypt.GenerateFromPassword(password, cost)
	if err != nil {
		log.Println(
			"[ERROR] error hashing data....",
		)
		return hash
	}

	return hash
}

//compare hash password and password

func Compare(hash, password []byte) bool {
	err := bcrypt.CompareHashAndPassword(hash, password)
	return err == nil
}

// //bcrypthash hashes password using bcrypt
// type bcryptHash struct {
// 	cost int
// }

// //newbcrypthash return new bcrypt
// func NewBcryptHash(cost int) Hasher {
// 	return &bcryptHash{cost: cost}
// }

// //hash password
// func (bc *bcryptHash) Hash(password []byte) []byte {
// 	hash, err := bcrypt.GenerateFromPassword(password, bc.cost)
// 	if err != nil {
// 		log.Println(
// 			"[ERROR] error hashing data....",
// 		)
// 		return hash
// 	}

// 	return hash
// }

// //compare hash password and password

// func (bc *bcryptHash) Compare(hash, password []byte) bool {
// 	err := bcrypt.CompareHashAndPassword(hash, password)
// 	return err == nil
// }
