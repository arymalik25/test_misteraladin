package server

import (
	"database/sql"
	"fmt"
	"log"
	"strconv"

	mdl "misteraladin/util/model"

	_ "github.com/go-sql-driver/mysql"
)

type dbReadWriter struct {
	db *sql.DB
}

const (
	addshortlink    = `INSERT INTO mst_shorturl (link_address, link_short, status) values (?,?,?)`
	deleteshortlink = `UPDATE mst_shorturl set status = ? where link_short = ?`
	getshorturl     = `SELECT link_address FROM mst_shorturl where link_short = ? and status = ?`
)

func NewReadWriter(url string, schema string, user string, password string) ReadWriter {
	fmt.Println(url, schema, user, password)
	schemaURL := fmt.Sprintf("%s:%s@tcp(%s)/%s?parseTime=true&readTimeout=360s", user, password, url, schema)
	db, err := sql.Open("mysql", schemaURL)
	if err != nil {
		log.Println("error schema : ", err)
	}
	var version string
	db.QueryRow("SELECT VERSION()").Scan(&version)
	return &dbReadWriter{db: db}
}

func (rw *dbReadWriter) WriteShortLink(linkaddress string, linkshort string) error {

	tx, err := rw.db.Begin()
	if err != nil {
		return err
	}
	defer tx.Rollback()

	_, nerr := tx.Exec(addshortlink, linkaddress, linkshort, mdl.Active)
	if nerr != nil {
		return nerr
	}

	return tx.Commit()
}

func (rw *dbReadWriter) DeleteShortLink(linkaddress string, linkshort string) error {
	tx, err := rw.db.Begin()
	if err != nil {
		return err
	}
	defer tx.Rollback()

	_, nerr := tx.Exec(deleteshortlink, mdl.Delete, linkshort)
	if nerr != nil {
		return err
	}

	return tx.Commit()
}
func (rw *dbReadWriter) GetShortlink(linkshort string) ShortLink {
	fmt.Println(strconv.Quote(linkshort))
	resp := ShortLink{}
	var link string
	err := rw.db.QueryRow(getshorturl, linkshort, mdl.Active).Scan(&link)
	if err != nil {
		fmt.Println(err)
		return resp
	}
	resp.LinkAddress = link

	return resp
}
