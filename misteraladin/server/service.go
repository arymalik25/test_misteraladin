package server

import (
	"net/http"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
)

const (
	lengthurl = 4
)

type ShortLink struct {
	LinkAddress string
	LinkShort   string
}

type JWT struct {
	Token   string
	Expired time.Time
}

//created struct to read the username and password from the request body
type Credentials struct {
	Password string `json:"password"`
	Username string `json:"username"`
}

// Create the JWT key used to create the signature
var jwtKey = []byte("misteraladin")

//create a struct that will be encoded to a jwt
//we add jwt.standardclaims as an embedded type, to provide fields like expiry time
type Claims struct {
	Username string `json:"username"`
	jwt.StandardClaims
}

//example user
var users = map[string]string{
	"user1": "password1",
	"user2": "password2",
}

type ReadWriter interface {
	WriteShortLink(string, string) error
	DeleteShortLink(string, string) error
	GetShortlink(string) ShortLink
}

type ShortUrlService interface {
	SubmitShortUrl(w http.ResponseWriter, r *http.Request)
	DeleteShortUrl(w http.ResponseWriter, r *http.Request)
	GetShortUrl(w http.ResponseWriter, r *http.Request)
	Signin(w http.ResponseWriter, r *http.Request)
}
