package server

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	gen "misteraladin/util/generate"
	"net/http"
	"strings"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
)

type shorturl struct {
	writer ReadWriter
	geturl string
}

func NewShortUrlService(writer ReadWriter, geturl string) ShortUrlService {
	return &shorturl{writer: writer, geturl: geturl}
}

func (su *shorturl) SubmitShortUrl(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")

	VerifyToken(r)

	// var token string
	// var ok bool
	// if ok, token = checkSession(w, r); !ok {
	// 	http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
	// 	return
	// }

	readBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	req := ShortLink{}
	err = json.Unmarshal(readBody, &req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	ranstring := gen.RandStringBytesRmndr(lengthurl)
	fmt.Println(ranstring)
	req.LinkShort = ranstring

	err = su.writer.WriteShortLink(req.LinkAddress, req.LinkShort)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	send, err := json.Marshal(req.LinkShort)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Write(send)

}

func (su *shorturl) DeleteShortUrl(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Access-Control-Allow-Origin", "*")

	readBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	req := ShortLink{}
	err = json.Unmarshal(readBody, &req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = su.writer.DeleteShortLink(req.LinkAddress, req.LinkShort)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

}
func (su *shorturl) GetShortUrl(w http.ResponseWriter, r *http.Request) {
	defer func() {
		if r.Body != nil {
			r.Body.Close()
		}
	}()

	//only set these for development only
	w.Header().Set("Access-Control-Allow-Headers", "*")

	url := r.URL.Path
	distr := strings.Replace(url, su.geturl, "", 1)
	if distr == "" {
		http.Error(w, "request data is null", http.StatusBadRequest)
		return
	}

	// split string s2 to []string
	sreq := strings.Split(distr, ",")

	data := su.writer.GetShortlink(sreq[0])

	// send, err := json.Marshal(data.LinkAddress)
	// if err != nil {
	// 	http.Error(w, err.Error(), http.StatusInternalServerError)
	// 	return
	// }

	// w.Write(send)

	redirect(data.LinkAddress, w, r)

}

func redirect(link string, w http.ResponseWriter, r *http.Request) {

	http.Redirect(w, r, link, 301)
}

// Create the Signin handler
func (su *shorturl) Signin(w http.ResponseWriter, r *http.Request) {
	var creds Credentials
	// Get the JSON body and decode into credentials
	err := json.NewDecoder(r.Body).Decode(&creds)
	if err != nil {
		// If the structure of the body is wrong, return an HTTP error
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	// Get the expected password from our in memory map
	expectedPassword, ok := users[creds.Username]

	// If a password exists for the given user
	// AND, if it is the same as the password we received, the we can move ahead
	// if NOT, then we return an "Unauthorized" status
	if !ok || expectedPassword != creds.Password {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	// Declare the expiration time of the token
	// here, we have kept it as 5 minutes
	expirationTime := time.Now().Add(5 * time.Minute)
	// Create the JWT claims, which includes the username and expiry time
	claims := &Claims{
		Username: creds.Username,
		StandardClaims: jwt.StandardClaims{
			// In JWT, the expiry time is expressed as unix milliseconds
			ExpiresAt: expirationTime.Unix(),
		},
	}

	// Declare the token with the algorithm used for signing, and the claims
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	// Create the JWT string
	tokenString, err := token.SignedString(jwtKey)
	if err != nil {
		// If there is an error in creating the JWT return an internal server error
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	// Finally, we set the client cookie for "token" as the JWT we just generated
	// we also set an expiry time which is the same as the token itself
	// http.SetCookie(w, &http.Cookie{
	// 	Name:    "token",
	// 	Value:   tokenString,
	// 	Expires: expirationTime,
	// })

	resp := JWT{
		Token:   tokenString,
		Expired: expirationTime,
	}

	fmt.Println(resp)

	send, err := json.Marshal(resp)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Write(send)
}

func VerifyToken(r *http.Request) bool {

	reqToken := r.Header.Get("Authorization")
	req := strings.Split(reqToken, " ")
	token, err := jwt.Parse(req[1], func(t *jwt.Token) (interface{}, error) {
		return []byte(jwtKey), nil
	})
	if err == nil && token.Valid {
		fmt.Println("valid token", err)
		return true
	} else {
		fmt.Println("invalid token", err)
		return false
	}
}
