package main

import (
	"fmt"
	"log"
	"net/http"

	sv "misteraladin/server"
	cfg "misteraladin/util/config"
)

func main() {
	ok := cfg.OpenConfig.LoadConfig()
	if !ok {
		log.Println(ok)
		return
	}

	//if not get then its nill string
	port := cfg.Get("serviceport", "8000")
	//ip := cfg.Get("serviceip", "")
	dbhost := cfg.Get("dbhost", "")
	dbname := cfg.Get("dbschema", "")
	dbuser := cfg.Get("dbbuid", "")
	dbpass := cfg.Get("dbpass", "")
	submit := cfg.Get("submiturl", "")
	delete := cfg.Get("deleteurl", "")
	get := cfg.Get("geturl", "")

	fmt.Println("running config", port, dbhost, dbname, dbuser, dbpass, submit, delete, get)

	readwriter := sv.NewReadWriter(dbhost, dbname, dbuser, dbpass)
	service := sv.NewShortUrlService(readwriter, get)

	http.HandleFunc(submit, service.SubmitShortUrl)
	http.HandleFunc(delete, service.DeleteShortUrl)
	http.HandleFunc(get, service.GetShortUrl)
	http.HandleFunc("/signin", service.Signin)

	err := http.ListenAndServe(":"+port, nil)
	if err != nil {
		log.Println("Error!!! cannot start the server : ", err)
		return
	}

}
